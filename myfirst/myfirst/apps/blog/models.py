from django.db import models


# Create your models here.
class Article(models.Model):
    title = models.CharField('Article Title', max_length=200)
    body = models.TextField('Article body')
    dateCreated = models.DateTimeField('Date created')


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author = models.CharField('Author name', max_length=50)
    body = models.TextField('Comment body')
